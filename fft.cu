#include <stdio.h>
#include <assert.h>
#include "cuda.h"
#include "cuda_runtime.h"
#include <cmath>

// thrust libs
#include <thrust/device_vector.h>
#include <thrust/transform.h>

#include "fft.h"
//#include "imaging.h"

// Helper functions for CUDA
#include <helper_functions.h>
#include <helper_cuda.h>

// static vars for intermediate results
// I'm leaving these as globals now for 2 reasons:
// 1. To avoid memory thrashing creating and deleting mem  at 30fps
// 2. I can only process one set at a time with the current set-up (not taking advantage of multiple cards)
static cufftHandle plan, invPlan;
static cufftComplex *idata;
static cufftComplex *odata;
static cufftComplex *rdata, *gdata, *bdata, *gscale;
// inverse buffers
static cufftComplex *rifft, *gifft, *bifft;
static long xy;

// forwards
__global__
void fftShift(cufftComplex *data, unsigned nX, unsigned nY);
__global__
void ifftShift(cufftComplex *data, unsigned nX, unsigned nY, float scale);
// filter
__global__
void runFilter(cufftComplex* source, int pX, int pY, int C1, int C2, bool inverted);


// IMPL
void initFFT(int x, int y) {
    xy = x*y;
    checkCudaErrors(cudaMalloc((void**)&idata, sizeof(cufftComplex) * xy));
    checkCudaErrors(cudaMalloc((void**)&odata, sizeof(cufftComplex) * xy));
    rdata = (cufftComplex*)calloc(sizeof(cufftComplex), xy);
    gdata = (cufftComplex*)calloc(sizeof(cufftComplex), xy);
    bdata = (cufftComplex*)calloc(sizeof(cufftComplex), xy);
    gscale = (cufftComplex*)calloc(sizeof(cufftComplex), xy);
    // inverse buffers
    rifft = (cufftComplex*)calloc(sizeof(cufftComplex), xy);
    gifft = (cufftComplex*)calloc(sizeof(cufftComplex), xy);
    bifft = (cufftComplex*)calloc(sizeof(cufftComplex), xy);

    // create 2d plan
    checkCudaErrors(cufftPlan2d(&plan, x, y, CUFFT_C2C));
    checkCudaErrors(cufftPlan2d(&invPlan, x, y, CUFFT_C2C));
}

void closeFFT() {
    // destroy the plan
    cufftDestroy(plan);
    cufftDestroy(invPlan);

    // free memory
    cudaFree(idata);
    cudaFree(odata);
    free(rdata);
    free(gdata);
    free(bdata);
}


void fftDo(cufftComplex* source, int x, int y, float* dest, cufftComplex* ifft, BandFilter* cFilter, int scaling) {
    bool bGotSource = false;
    cudaMemset(idata, 0x00, sizeof(cufftComplex)*xy);
    cudaMemset(odata, 0x00, sizeof(cufftComplex)*xy);

    checkCudaErrors(cudaMemcpy(idata, source, sizeof(cufftComplex)*xy, cudaMemcpyHostToDevice));
    // shift the data
    fftShift<<<x,y>>>(idata, x, y);
    // transform data
    checkCudaErrors(cufftExecC2C(plan, idata, odata, CUFFT_FORWARD));
    // do we inverse it?
    if(ifft != 0) {
        // only do filter if going to inverse otherwise it's a pointless operation.
        if(cFilter && cFilter->isOn) {
            runFilter<<<x,y>>>(odata, x, y, cFilter->C1 * cFilter->C1, cFilter->C2 * cFilter->C2, cFilter->isInverted != 0);
        }
        // get data out of cuda buffer for display
        checkCudaErrors(cudaMemcpy(source, odata, sizeof(cufftComplex)*xy, cudaMemcpyDeviceToHost));
        bGotSource = true;
        // transform data
        checkCudaErrors(cufftExecC2C(plan, odata, idata, CUFFT_INVERSE));
        // un-shift and scale the data
        ifftShift<<<x,y>>>(idata, x, y, xy);
        // get inverse fft out of cuda buffer
        checkCudaErrors(cudaMemcpy(ifft, idata, sizeof(cufftComplex)*xy, cudaMemcpyDeviceToHost));
    }
    if(!bGotSource) {
        // get data out of cuda buffer
        checkCudaErrors(cudaMemcpy(source, odata, sizeof(cufftComplex)*xy, cudaMemcpyDeviceToHost));
        bGotSource = true;
    }
    memset(dest, 0x00, sizeof(float)*xy);
    // get the phase to display image
    float max = 0;
    // change output based on scaling param
    switch(scaling) {
    case 0 :
    default :
        for(int i = 0; i < xy; i++) {
            float val = 100 * log(1.0 + cuCabsf(source[i]));
            dest[i] = val;
            if(val > max) {
                max = val;
            }
        }
        break;
    case 1 :
        for(int i = 0; i < xy; i++) {
            float val = 100 * cuCabsf(source[i]);
            dest[i] = val;
            if(val > max) {
                max = val;
            }
        }
        break;
    }
    for(int i = 0; i < xy; i++) {
        dest[i] /= max;
    }
}


// TODO this needs to be reworked into doing all three at the same time using all available cuda cards
void fftImage(LiveSOM* som) {
    rgb* snapshot = (rgb*)som->snapshotData;
    int x = som->somSides;
    int y = som->somSides;
    float* destR = som->redBufferData;
    float* destG = som->greenBufferData;
    float* destB = som->blueBufferData;
    float* destGS = som->greyBufferData;
    rgb* destInv = (rgb*)som->inverseData;

    memset(rdata, 0x00, sizeof(cufftComplex)*xy);
    memset(gdata, 0x00, sizeof(cufftComplex)*xy);
    memset(bdata, 0x00, sizeof(cufftComplex)*xy);
    memset(gscale, 0x00, sizeof(cufftComplex)*xy);

    for(int i = 0; i < xy; i++) {
        rdata[i].x = snapshot[i].r;
        gdata[i].x = snapshot[i].g;
        bdata[i].x = snapshot[i].b;

        // greyscale
        gscale[i].x = 0.2126*snapshot[i].r + 0.587*snapshot[i].g + 0.114*snapshot[i].b;
    }

    // process the red one
    fftDo(rdata, x, y, destR, rifft, &som->redCFilter, som->scaling);
    // green
    fftDo(gdata, x, y, destG, gifft, &som->greenCFilter, som->scaling);
    // blue
    fftDo(bdata, x, y, destB, bifft, &som->blueCFilter, som->scaling);
    // recombine the three color inverses into full color inverse
    for(int i = 0; i < xy; i++) {
        destInv[i].r = rifft[i].x;
        destInv[i].g = gifft[i].x;
        destInv[i].b = bifft[i].x;
    }
    // grey scale
    fftDo(gscale, x, y, destGS, 0, NULL, som->scaling);
}

__global__
void fftShift(cufftComplex *data, unsigned nX, unsigned nY)
{
    unsigned int tid = threadIdx.x + (blockIdx.x * blockDim.x);
    ushort x = tid % nX;
    ushort y = (tid - x) / nX;

    if (x < nX && y < nY) {
           unsigned index = y * nX + x;
           float a = 1-2*((x+y)&1);
           data[index].x *= a;
           data[index].y *= a;
    }
}


// unshift and scale down
__global__
void ifftShift(cufftComplex *data, unsigned nX, unsigned nY, float scale)
{
    unsigned int tid = threadIdx.x + (blockIdx.x * blockDim.x);
    ushort x = tid % nX;
    ushort y = (tid - x) / nX;

    if (x < nX && y < nY) {
           unsigned index = y * nX + x;
           float a = (1-2*((x+y)&1)) /scale;
           data[index].x *= a;
           data[index].y *= a;
    }
}

// polar angle
__host__ __device__
float carg(const cuComplex& z) {return atan2(cuCimagf(z), cuCrealf(z));}
__host__ __device__
cuComplex cp2c(const float d, const float a) {return make_cuFloatComplex(d*cos(a), d*sin(a));}

__global__
void runFilter(cufftComplex* source, int pX, int pY, int C1, int C2, bool inverted) {
    unsigned int tid = threadIdx.x + (blockIdx.x * blockDim.x);
    ushort x = tid % pX;
    ushort y = (tid - x) / pX;
    bool boutsideRegion;

    if( x < pX && y < pY ) {
        float dx = abs(x - pX/2);
        dx *= dx;
        float dy = abs(y - pY/2);
        dy *= dy;
        boutsideRegion = !(((dx+dy) >= C1) && ((dx+dy) <= C2));
        if( (!inverted && boutsideRegion) || (inverted && !boutsideRegion) ) {
            int loc = x + y * pX;
            cufftComplex src = source[loc];
            source[loc] = cp2c(0, carg(src));
        }
    }
}
//EOF
