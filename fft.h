#ifndef __FFT_H__
#define __FFT_H__
/*
	header file to hold the cpu function declarations to be called by the go code
*/
#include "cufft.h"


typedef struct s_rgb {
	char r;
	char g;
	char b;
} rgb;

// forward
typedef struct BandFilter_t {
    char isOn;
    char isInverted;
    int C1, C2;
} BandFilter;
typedef struct SOM_Element_t{
    ushort x, y, z;
    unsigned char r,g,b,a;
} SOM_Element;

typedef struct LiveSOM_t {
    ushort somSides;
    int somSize;
    char* snapshotData;
    float* redBufferData;
    float* greenBufferData;
    float* blueBufferData;
    float* greyBufferData;
    char* inverseData;

    SOM_Element* points;
    void* lastFFT;		// internally maps to a clock_t

    // filter vars
    BandFilter redCFilter;
    BandFilter greenCFilter;
    BandFilter blueCFilter;

    // scaling
    int scaling;

} LiveSOM;


void fftImage(LiveSOM* som);
void initFFT(int x, int y);
void closeFFT();
void fftDo(cufftComplex* source, int x, int y, float* dest, cufftComplex* ifft, BandFilter* cFilter, int scaling);
#endif
