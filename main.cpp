

#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include <time.h>
#include <cuda_runtime_api.h>
#include <boost/lexical_cast.hpp>
#define ASYNC

//#define GET_CALL(buffer, channel) asyncFFT_getPhase(&(buffer), (channel))
#define GET_CALL(buffer, channel) asyncFFT_getInvFFT((buffer), (channel))
//#define GET_CALL(buffer, channel) asyncFFT_getFFT((buffer), (channel))
#ifdef ASYNC
#include "asyncFFT.h"
#else
#include "fft.h"
#endif
int main()
{
    cv::VideoCapture cap;
    if(!cap.open("/home/dmoodie/Downloads/trailer.mp4"))
        return -1;
    cv::Mat img;
    cap.read(img);
    float* rPhase, *gPhase, *bPhase;
#ifdef ASYNC
    asyncFFT_init(img.cols, img.rows, 20, 3, "0", 3);
    asyncFFT_setAsync(false);
    asyncFFT_shiftFFT(false);
    unsigned char* rBuffer = nullptr;
    unsigned char* gBuffer = nullptr;
    unsigned char* bBuffer = nullptr;
    cudaMallocHost((void**)&rBuffer, sizeof(unsigned char)*img.rows*img.cols);
    cudaMallocHost((void**)&gBuffer, sizeof(unsigned char)*img.rows*img.cols);
    cudaMallocHost((void**)&bBuffer, sizeof(unsigned char)*img.rows*img.cols);
#else
    cudaSetDevice(0);
    initFFT(img.cols,img.rows);
    cufftComplex* rBuffer = nullptr;
    cufftComplex* gBuffer = nullptr;
    cufftComplex* bBuffer = nullptr;
    cudaMallocHost((void**)&rBuffer, sizeof(cufftComplex)*img.rows*img.cols);
    cudaMallocHost((void**)&gBuffer, sizeof(cufftComplex)*img.rows*img.cols);
    cudaMallocHost((void**)&bBuffer, sizeof(cufftComplex)*img.rows*img.cols);
    cufftComplex* riBuffer = nullptr;
    cufftComplex* giBuffer = nullptr;
    cufftComplex* biBuffer = nullptr;
    cudaMallocHost((void**)&riBuffer, sizeof(cufftComplex)*img.rows*img.cols);
    cudaMallocHost((void**)&giBuffer, sizeof(cufftComplex)*img.rows*img.cols);
    cudaMallocHost((void**)&biBuffer, sizeof(cufftComplex)*img.rows*img.cols);
#endif

    rPhase = new float[img.cols*img.rows];
    gPhase = new float[img.cols*img.rows];
    bPhase = new float[img.cols*img.rows];

    BandFilter filter;
    filter.C1 = 5;
    filter.C2 = 100;
    filter.isInverted = 0;
    filter.isOn = false;
    int imgCount = 0;
    while(cap.read(img))
    {
        // Split each channel
        std::vector<cv::Mat> channels;
        cv::split(img, channels);

#ifdef ASYNC
        for(int i = 0; i < img.rows*img.cols; ++i)
        {
            rBuffer[i] = channels[0].at<uchar>(i);
            gBuffer[i] = channels[1].at<uchar>(i);
            bBuffer[i] = channels[2].at<uchar>(i);
        }
        auto C1 = clock();
        asyncFFT_inputPixelData(rBuffer,0,true, &filter,0);
        auto C2 = clock();
        asyncFFT_inputPixelData(gBuffer,1,true, &filter,0);
        auto C3 = clock();
        asyncFFT_inputPixelData(bBuffer,2,true, &filter,0);
        auto C4 = clock();
        if(!GET_CALL(rPhase,0))
        {
            std::cout << "Channel 0 not ready yet" << std::endl;
            continue;
        }
        if(!GET_CALL(gPhase,1))
        {
            std::cout << "Channel 1 not ready yet" << std::endl;
            continue;
        }
        if(!GET_CALL(bPhase,2))
        {
            std::cout << "Channel 2 not ready yet" << std::endl;
            continue;
        }
        std::cout << "Dispatching channels took: " << 1000*float(C2 - C1)/CLOCKS_PER_SEC << " " << 1000*float(C3 - C2)/CLOCKS_PER_SEC << " " << 1000*float(C4 - C3)/CLOCKS_PER_SEC << std::endl;
        cv::Mat rPhase_(cv::Size(img.cols,img.rows), CV_32F, rPhase);
        cv::Mat gPhase_(cv::Size(img.cols,img.rows), CV_32F, gPhase);
        cv::Mat bPhase_(cv::Size(img.cols,img.rows), CV_32F, bPhase);
#else
        for(int i = 0; i < img.rows*img.cols; ++i)
        {
            rBuffer[i].x = channels[0].at<uchar>(i);
            gBuffer[i].x = channels[1].at<uchar>(i);
            bBuffer[i].x = channels[2].at<uchar>(i);
        }
        auto C1 = clock();
        fftDo(rBuffer,img.cols, img.rows, rPhase, riBuffer, &filter,0);
        auto C2 = clock();
        fftDo(gBuffer,img.cols, img.rows, gPhase, giBuffer, &filter,0);
        auto C3 = clock();
        fftDo(bBuffer,img.cols, img.rows, bPhase, biBuffer, &filter,0);
        auto C4 = clock();
        std::cout << "Dispatching channels took: " << 1000*float(C2 - C1)/CLOCKS_PER_SEC << " " << 1000*float(C3 - C2)/CLOCKS_PER_SEC << " " << 1000*float(C4 - C3)/CLOCKS_PER_SEC << std::endl;
        cv::Mat rPhase_(cv::Size(img.cols,img.rows), CV_32FC2, riBuffer);
        cv::Mat gPhase_(cv::Size(img.cols,img.rows), CV_32FC2, giBuffer);
        cv::Mat bPhase_(cv::Size(img.cols,img.rows), CV_32FC2, biBuffer);
        cv::split(rPhase_, channels);
        rPhase_ = channels[0];
        cv::split(gPhase_, channels);
        gPhase_ = channels[0];
        cv::split(bPhase_, channels);
        bPhase_ = channels[0];
#endif
        double minVal, maxVal, range, scaleFactor;
        cv::minMaxIdx(rPhase_,&minVal,&maxVal,0,0);
        range = maxVal - minVal;
        scaleFactor = 255.0 / range;
        cv::convertScaleAbs(rPhase_,rPhase_, scaleFactor, minVal*scaleFactor);
        cv::minMaxIdx(gPhase_,&minVal,&maxVal,0,0);
        range = maxVal - minVal;
        scaleFactor = 255.0 / range;
        cv::convertScaleAbs(gPhase_,gPhase_, scaleFactor, minVal*scaleFactor);
        cv::minMaxIdx(bPhase_,&minVal,&maxVal,0,0);
        range = maxVal - minVal;
        scaleFactor = 255.0 / range;
        cv::convertScaleAbs(bPhase_,bPhase_, scaleFactor, minVal*scaleFactor);
        ++imgCount;
        std::vector<cv::Mat> planes;
        planes.push_back(rPhase_);
        planes.push_back(gPhase_);
        planes.push_back(bPhase_);
        cv::Mat merged;
        cv::merge(planes, merged);
        cv::imshow("Merged inv", merged);
        cv::imshow("Image",img);
        cv::waitKey(1);
    }
    return 0;
}

