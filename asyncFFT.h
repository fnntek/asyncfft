#pragma once




/*	Async FFT is initialized with the number of GPU cards to use, the number of concurrent processes to run, and buffer size.
 *  It then can be fed data via the inputData function in the form of a float*, which is then asynchronously uploaded to the GPU, FFT'd
 *  and then asynchronously downloaded into a locally owned buffer. GetData then gets a pointer to the oldest value in the buffer.
 */
#include "cufft.h"
#ifdef _WIN64
	#ifdef EXPORTING
		#define DllExport   __declspec( dllexport )
	#else
		#define DllExport   __declspec( dllimport )
	#endif
	#define CALL __stdcall
#else
	#define DllExport
	#define CALL
#endif

#ifdef __cplusplus
	extern "C" {
#endif

typedef struct BandFilter_t {
	char isOn;
	char isInverted;
	int C1, C2;
} BandFilter;
	// C bindings for go

	/*	\brief asyncFFT_init initializes the FFT
	 * 	\param width is the width of incoming images
	 * 	\param height is the height of incoming images
	 * 	\param bufferSize is the number of images to store in a circular buffer for each channel
     * 	\param GPUs is a comma separated string of which GPUs to use, IE: 0 or 0,1 or 2,3,4
	 * 	\param channels is the number of channels to expect.  Which thus determines the number of buffers
	 *	\return true on success, false on any kind of failure
	 */
    DllExport char CALL asyncFFT_init(int width, int height, int h_buffers, int d_buffers, const char* GPUs, int channels);

	DllExport char CALL asyncFFT_close();
	/*	\brief makes a copy of input data on the CPU, then uploads the data on the processing queue
		 *	\param data is a pointer to the data of width*height for which the FFT is to be performed
		 *	\param channel is the channel that this data is associated with
		 */
	DllExport char CALL asyncFFT_inputData(cufftComplex* data, int channel, char calcInverse, BandFilter* cFilter, int scaling);
    DllExport char CALL asyncFFT_inputPixelData(unsigned char* data, int channel, char calcInverse, BandFilter* cFilter, int scaling);

	//	\brief updates your pointer to a pointer to the FFT at the back of the queue for this channel

	DllExport char CALL asyncFFT_getFFT(float* data, int channel);

	// \brief updates your pointer to a pointer to the inv FFT at the back of the queue for this channel
	DllExport char CALL asyncFFT_getInvFFT(float* data, int channel);

    // Updates a pointer to the host side buffer of the oldest phase result.
	DllExport char CALL asyncFFT_getPhase(float** data, int channel);
    // Makes a copy of the oldest host side buffer of phase result
	DllExport char CALL asyncFFT_getPhaseCopy(float* data, int channel);
    // Boolean toggle to perform a shift in the FFT or not to perform a shift.
    DllExport char CALL asyncFFT_shiftFFT(char val);
    DllExport char CALL asyncFFT_setAsync(char val);
#ifdef __cplusplus
	}
#endif

