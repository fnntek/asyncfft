
#include "asyncFFT.h"

#include "cuda.h"
#include "cuda_runtime.h"
#include "helper_cuda.h"
#include <thrust/device_vector.h>
#include <thrust/transform.h>

#include <algorithm>
#include <memory>
#include <deque>
#include <vector>
#include <type_traits>
#include <sstream>
#include <time.h>

//#define _DEBUG
#define cudaDeviceScheduleBlockingSync   0x04
#define SHIFT
//#define _DEBUG
// Currently broken, causes either cudaErrorUnknown or segfault when trying to record an event.
//#define USE_EVENTS 1


#ifdef _DEBUG
#define SYNC checkCudaErrors(cudaStreamSynchronize(buffers[currentBuffer]->stream));
#else
#define SYNC if(!asyncEnabled) checkCudaErrors(cudaStreamSynchronize(buffers[currentBuffer]->stream));
#endif

//
namespace AsyncFFT
{
template<typename T>
typename std::enable_if<std::is_pointer<T>::value, void>::type cleanup(const T& value)
{
    cudaFreeHost(value);
}
template<typename T>
typename std::enable_if<!std::is_pointer<T>::value, void>::type cleanup(const T& value)
{

}

template<typename T> class ConstBuffer
{
    unsigned long int putItr;
    unsigned long int getItr;
    unsigned long int size;
    std::vector<T> buffer;
public:
    ConstBuffer(int size_, const T& init)
    {
        putItr = 0;
        getItr = 0;
        size = size_;
        buffer.resize(size_, init);
    }
    ~ConstBuffer()
    {
        for(int i = 0; i < size; ++i)
        {
            cleanup(buffer[i]);
        }
    }
    bool backValid(){ return getItr < putItr;	}
    T& getFront()
    {
        // Throw exception if writing overlaps reading
        if(getItr > putItr && getItr % size == putItr % size)
            throw std::string("Queue full!");
        return buffer[putItr++ % size];
    }
    T& getBack()
    {
        // Throw exception if reading catches up to writing
        if(getItr == putItr)
            throw std::string("Nothing to be read");
        std::cout << getItr;
        return buffer[getItr++ % size];
    }
    size_t getBacklogSize()	{return getItr % size - putItr % size;	}
};

template<typename T>class ConstCudaBuffer
{
#if USE_EVENTS
    std::vector<cudaEvent_t> 	filledEvents;
#else
    std::vector<cudaStream_t>   filledStreams;
#endif
    std::vector<T>				buffer;
    unsigned long int putItr;
    unsigned long int getItr;
    unsigned long int size;
public:
    ConstCudaBuffer(int size_, const T& init)
    {
        putItr = 0;
        getItr = 0;
        size = size_;
        buffer.resize(size_, init);
#if USE_EVENTS
        filledEvents.resize(size_, 0);
        for(int i = 0; i < filledEvents.size(); ++i)
        {
            cudaEventCreate(&filledEvents[i]);
        }
#else
        filledStreams.resize(size_,0);
#endif
    }
    ~ConstCudaBuffer()
    {
        for(int i = 0; i < size; ++i)
            cleanup(buffer[i]);
#if USE_EVENTS
        for(int i = 0; i < size; ++i)
            cudaEventDestroy(filledEvents[i]);
#endif
    }
#if USE_EVENTS
    T* getFront(cudaEvent_t& fillEvent)
    {
        fillEvent = filledEvents[putItr%size];
        return &buffer[putItr++ % size];

    }
#else
    T* getFront(cudaStream_t** fillStream)
    {
        *fillStream = &filledStreams[putItr%size];
        return &buffer[putItr++ % size];
    }

#endif
    T* getBack()
    {
        if(getItr == putItr)
        {
            std::cout << "getItr == putItr == " << getItr << std::endl;
            return nullptr;
        }
        // Check if it has been filled yet
#if USE_EVENTS
        if(cudaEventQuery(filledEvents[getItr%size]) != cudaSuccess)
        {
            std::cout << "Buffer not filled yet" << std::endl;
            return nullptr;
        }
#else
        if(cudaStreamQuery(filledStreams[getItr % size]) != cudaSuccess)
        {
            std::cout << "Buffer not filled yet" << std::endl;
            return nullptr;
        }
#endif
        //  std::cout << getItr%size;
        return &buffer[getItr++ % size];

    }
    size_t getBacklogSize()	{return putItr - getItr ;	}
    bool backValid(){ return getItr < putItr;	}
};



// ***********************************************************************************************************
// Handles the FFT for a single GPU
class asyncFFT
{
    asyncFFT();

    cufftHandle plan;
    unsigned char currentBuffer;
    unsigned int device;
    unsigned short width;
    unsigned short height;
    // Each buffer represents a stream worth of work that can be performed and all of the buffers needed for that stream.
    // Thus it contains a set of temporary image buffers and result buffers, as well as the stream that all operations
    // should be performed in
    struct buffer
    {
        buffer(int size)
        {
            // Allocate complex buffer
            checkCudaErrors(cudaMalloc((void**)&d_Cbuffer, sizeof(cufftComplex)*size));
            // Allocate pixel buffer
            checkCudaErrors(cudaMalloc((void**)&d_Pbuffer, sizeof(unsigned char)*size));
            // Allocate invFFT result buffer
            checkCudaErrors(cudaMalloc((void**)&d_InvBuffer, sizeof(cufftComplex)*size));
            // Allocate FFT result buffer
            checkCudaErrors(cudaMalloc((void**)&d_FFTBuffer, sizeof(cufftComplex)*size));
            // Allocate phase buffer
            checkCudaErrors(cudaMalloc((void**)&d_phaseBuffer,sizeof(float)*size));
            // Allocate a stream for this buffer object
            checkCudaErrors(cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking));
        }
        ~buffer()
        {
            checkCudaErrors(cudaStreamDestroy(stream));
            checkCudaErrors(cudaFree(d_Cbuffer));
            checkCudaErrors(cudaFree(d_Pbuffer));
            checkCudaErrors(cudaFree(d_InvBuffer));
            checkCudaErrors(cudaFree(d_FFTBuffer));
            checkCudaErrors(cudaFree(d_phaseBuffer));
        }
        // Holds incoming input images
        cufftComplex*   d_Cbuffer;
        // Pixel buffer for upload
        unsigned char*      d_Pbuffer;
        // Holds the inverse FFT
        cufftComplex* d_InvBuffer;
        // Holds the FFT
        cufftComplex* d_FFTBuffer;
        // Holds the phase output
        float* d_phaseBuffer;
        cudaStream_t stream;
    };
    // 	Under ideal situations we can perform one upload, one FFT and one download concurrently.
    //	So we need one GPU buffer for each situation
    std::vector< std::unique_ptr<buffer> > buffers;

public:
    asyncFFT(int width, int height, int device, int numBuffers);
    ~asyncFFT();
    bool asyncEnabled;
#if USE_EVENTS      // Unfortunately events keeps causing a segfault
    template<typename T> bool
    enqueueImage(T* h_image, cufftComplex* h_resultBuffer, cufftComplex* h_invResultBuffer,
                      BandFilter* cFilter, float* h_phase, int scaling, cudaEvent_t fftFill, cudaEvent_t invFill, cudaEvent_t phaseFill, bool shift);
#else
    template<typename T> bool
    enqueueImage(T* h_image, cufftComplex* h_resultBuffer, cufftComplex* h_invResultBuffer,
                 BandFilter* cFilter, float* h_phase, int scaling, cudaStream_t& stream, bool shift);
#endif




};
// ***********************************************************************************************************
class FFTDispatcher
{
    FFTDispatcher();
    // Vector of objects representing each GPU
    std::vector<std::unique_ptr<asyncFFT> > GPUs;

    // Vector of host buffers for each channel
    std::vector< ConstCudaBuffer<cufftComplex*> > 	h_resultBuffers;
    std::vector< ConstCudaBuffer<cufftComplex*> > 	h_invResultBuffers;
    std::vector< ConstCudaBuffer<float*>        >	h_phaseBuffers;
    bool b_shiftFFT;
public:
    unsigned short width;
    unsigned short height;
    FFTDispatcher(int width, int height, std::vector<int> GPUs, int channels, int h_buffers, int d_buffers);
    template<typename T> bool dispatchImage(T* h_data, int channel, bool calcInv, BandFilter* cFilter, int scaling);
    bool getFFT(float* h_fft, int channel);
    bool getInvFFT(float* h_invFFT, int channel);
    bool getPhase(float** h_phase, int channel);
    bool shiftFFT(bool val);
    bool setAsync(bool val);
};

// forwards
__global__ void
fftShift(cufftComplex *data, unsigned nX, unsigned nY);

__global__ void
ifftShift(cufftComplex *data, unsigned nX, unsigned nY, float scale);

// filter
template<bool inverted> __global__ void
runFilter(cufftComplex* source, int pX, int pY, int C1, int C2);

template<int scaling> __global__ void
getPhase(cufftComplex* source, float* phase);

__global__ void
convertU8toF32C2(unsigned char* pixels, cufftComplex* output);

template<typename T0, typename T1>__global__ void
scaleImage(T0* image, T1 rows, T1 cols, T0 minVal);

// ************************************************************************************************************
//											Async FFT
// ************************************************************************************************************

asyncFFT::asyncFFT(){}

asyncFFT::asyncFFT(int width_, int height_, int device_, int numBuffers):
    device(device_), currentBuffer(0), width(width_), height(height_)
{
    checkCudaErrors(cudaSetDevice(device));
    cufftPlan2d(&plan, width, height, CUFFT_C2C);
    //buffers.reserve(numBuffers);
    for(int i =0; i < numBuffers; ++i)
    {
        buffers.push_back(std::unique_ptr<buffer>(new buffer(width*height)));
    }
    asyncEnabled = true;
}

asyncFFT::~asyncFFT()
{

}

// TODO: See what kind of error checking can be applied to async operations
template<typename T> bool
asyncFFT::enqueueImage(T* h_image, cufftComplex* h_resultBuffer, cufftComplex* h_invResultBuffer,
                       BandFilter* cFilter, float* h_phase, int scaling,
                       #if USE_EVENTS
                       cudaEvent_t fftFill, cudaEvent_t invFill, cudaEvent_t phaseFill, bool shift)
#else
                       cudaStream_t& stream, bool shift)
#endif
{
    static_assert(!std::is_same<T, cufftComplex>::value || !std::is_same<T,unsigned char>::value, "Can only handle cufftComplex and unsigned char inputs, given type is ");

    clock_t C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11, C12, C13;
    checkCudaErrors(cudaSetDevice(device));
    //std::cout << "Performing FFT on GPU# " << device << std::endl;
    C1 = clock();
    // 1 cycle
    cufftSetStream(plan, buffers[currentBuffer]->stream);
    C2 = clock();
#ifndef USE_EVENTS
    stream = buffers[currentBuffer]->stream;
#endif
    bool haveSource = false;
    // Copy image data into device buffer
    // Approx 1800 cycles
    if(std::is_same<T, cufftComplex>::value)
    {
        checkCudaErrors(cudaMemcpyAsync(buffers[currentBuffer]->d_Cbuffer,
                                        h_image,
                                        sizeof(cufftComplex)*width*height,
                                        cudaMemcpyHostToDevice,
                                        buffers[currentBuffer]->stream));
    }else
    {
        checkCudaErrors(cudaMemcpyAsync(buffers[currentBuffer]->d_Pbuffer,
                                        h_image, sizeof(T)*width*height,
                                        cudaMemcpyHostToDevice,
                                        buffers[currentBuffer]->stream));
        // Convert the dense pixel format to a sparse cufftComplex format
        convertU8toF32C2<<<width,height,0,buffers[currentBuffer]->stream>>>(
                    buffers[currentBuffer]->d_Pbuffer, buffers[currentBuffer]->d_Cbuffer);
    }

    SYNC
    C3 = clock();
    // approx 20 cycles
    if(shift)
        fftShift<<<width, height, 0, buffers[currentBuffer]->stream>>>(
                    buffers[currentBuffer]->d_Cbuffer,
                    width, height);

    SYNC

    C4 = clock();
    // Perform FFT
    // Approx 100 cycles
    checkCudaErrors(cufftExecC2C(plan,
                                 buffers[currentBuffer]->d_Cbuffer,
                                 buffers[currentBuffer]->d_FFTBuffer,
                                 CUFFT_FORWARD));
    SYNC
    C5 = clock();

    // if inverse is requested, calculate inverse
    // Approx 8 cycles
    if(h_invResultBuffer != 0)
    {
        if(cFilter && cFilter->isOn)
        {
            if(cFilter->isInverted != 0)
            {
                runFilter<true><<<width, height, 0, buffers[currentBuffer]->stream>>>(
                            buffers[currentBuffer]->d_FFTBuffer,
                            width, height,
                            cFilter->C1 * cFilter->C1,
                            cFilter->C2 * cFilter->C2
                            );
            }else
            {
                runFilter<false><<<width, height, 0, buffers[currentBuffer]->stream>>>(
                            buffers[currentBuffer]->d_FFTBuffer,
                            width, height,
                            cFilter->C1 * cFilter->C1,
                            cFilter->C2 * cFilter->C2
                            );
            }


        }
        SYNC
        C6 = clock();
        // approx 3500 cycles
        checkCudaErrors(cudaMemcpyAsync(h_resultBuffer,
                                        buffers[currentBuffer]->d_FFTBuffer,
                                        sizeof(cufftComplex)*width*height,
                                        cudaMemcpyDeviceToHost,buffers[currentBuffer]->stream));
        haveSource = true;
        SYNC
#ifdef USE_EVENTS
        // Segfault here
        checkCudaErrors(cudaEventRecord(fftFill, buffers[currentBuffer]->stream));
#endif
        C7 = clock();
        checkCudaErrors(cufftExecC2C(plan,
                                     buffers[currentBuffer]->d_FFTBuffer,
                                     buffers[currentBuffer]->d_InvBuffer,
                                     CUFFT_INVERSE));
        SYNC
        C8 = clock();

        if(shift)
            ifftShift<<<width,height,0,buffers[currentBuffer]->stream>>>(
                        buffers[currentBuffer]->d_InvBuffer,
                        width, height, width*height);
        SYNC
        C9 = clock();

        // approx 3100 cycles
        checkCudaErrors(cudaMemcpyAsync(h_invResultBuffer,
                                        buffers[currentBuffer]->d_InvBuffer,
                                        sizeof(cufftComplex)*width*height, cudaMemcpyDeviceToHost,
                                        buffers[currentBuffer]->stream));
        SYNC
        C10 = clock();

#ifdef USE_EVENTS
        checkCudaErrors(cudaEventRecord(invFill, buffers[currentBuffer]->stream));
#endif

    }

    if(!haveSource)
    {
        checkCudaErrors(cudaMemcpyAsync(h_resultBuffer,
                                        buffers[currentBuffer]->d_FFTBuffer,
                                        sizeof(cufftComplex)*width*height,
                                        cudaMemcpyDeviceToHost,buffers[currentBuffer]->stream));
#ifdef USE_EVENTS
        checkCudaErrors(cudaEventRecord(fftFill, buffers[currentBuffer]->stream));
#endif
        haveSource = true;
    }
    SYNC
    C11 = clock();

    if(scaling == 0)
        getPhase<0><<<width,height,0,buffers[currentBuffer]->stream>>>(
                    buffers[currentBuffer]->d_FFTBuffer,
                    buffers[currentBuffer]->d_phaseBuffer);
    if(scaling == 1)
        getPhase<1><<<width,height,0,buffers[currentBuffer]->stream>>>(
                    buffers[currentBuffer]->d_FFTBuffer,
                    buffers[currentBuffer]->d_phaseBuffer);

    scaleImage<float, unsigned short><<<1,height, sizeof(float)*height, buffers[currentBuffer]->stream>>>(
                buffers[currentBuffer]->d_phaseBuffer, height, width, std::numeric_limits<float>::min());
    SYNC
    C12 = clock();

    checkCudaErrors(cudaMemcpyAsync(h_phase,
                                    buffers[currentBuffer]->d_phaseBuffer,
                                    sizeof(float)*width*height,
                                    cudaMemcpyDeviceToHost,
                                    buffers[currentBuffer]->stream));
    SYNC
    C13 = clock();
#ifdef USE_EVENTS
    checkCudaErrors(cudaEventRecord(phaseFill, buffers[currentBuffer]->stream));
#endif
    //#define MS_SCALE( args ) 1000*float( (args) ) / CLOCKS_PER_SEC
#define MS_SCALE( args ) (args)
    //std::cout << (unsigned int)device << ": " << MS_SCALE(C2 - C1) << " " << MS_SCALE(C3 - C2) << " " << MS_SCALE(C4 - C3) << " " << MS_SCALE(C5 - C4) << " " << MS_SCALE(C6 - C5) << " " << MS_SCALE(C7 - C6) << " " << MS_SCALE(C8 - C7) << " " << MS_SCALE(C9 - C8) << " " << MS_SCALE(C10 - C9) << " " << MS_SCALE(C11 - C10) << " " << MS_SCALE(C12 - C11) << " " << MS_SCALE(C13 - C12) << std::endl;
    ++currentBuffer;
    if(currentBuffer == buffers.size())
        currentBuffer = 0;
    return true;
}

// ************************************************************************************************************
//											FFT Dispatcher
// ************************************************************************************************************
FFTDispatcher::FFTDispatcher()
{

}

FFTDispatcher::FFTDispatcher(int width_, int height_, std::vector<int> GPUs_, int channels, int h_buffers, int d_buffers):
    width(width_), height(height_)
{
    // One host result buffer for the FFT, invFFT, and phase output.
    h_resultBuffers.resize(channels,    ConstCudaBuffer<cufftComplex*>(h_buffers, nullptr));
    h_invResultBuffers.resize(channels, ConstCudaBuffer<cufftComplex*>(h_buffers, nullptr));
    h_phaseBuffers.resize(channels,     ConstCudaBuffer<float*>(h_buffers, nullptr));
    b_shiftFFT = true;
    // Setup the GPU object to handle device buffers and selection
    for(int i = 0; i < GPUs_.size(); ++i)
    {
        GPUs.push_back(std::unique_ptr<asyncFFT>(new asyncFFT(width, height, GPUs_[i], d_buffers)));
    }
}
template<typename T> bool FFTDispatcher::dispatchImage(T* data, int channel, bool calcInv, BandFilter* cFilter, int scaling)
{
    // Determine which GPU is currently least loaded

    // For now we're just using a rolling utilization scheme.
    static unsigned char currentDevice = 0;
#if USE_EVENTS
    cudaEvent_t fftFillEvent, invFillEvent, phaseFillEvent;
    cufftComplex** h_resultBuffer 		= h_resultBuffers[channel].getFront(fftFillEvent);
    cufftComplex** h_invResultBuffer 	= h_invResultBuffers[channel].getFront(invFillEvent);
    float**        h_phaseBuffer 		= h_phaseBuffers[channel].getFront(phaseFillEvent);
#else
    // Pointer to the location of each of the streams in the buffers
    cudaStream_t*   fftFillStream, *invFillStream, *phaseFillStream;

    cufftComplex** h_resultBuffer 		= h_resultBuffers[channel].getFront(&fftFillStream);
    cufftComplex** h_invResultBuffer 	= h_invResultBuffers[channel].getFront(&invFillStream);
    float**        h_phaseBuffer 		= h_phaseBuffers[channel].getFront(&phaseFillStream);

    cudaStream_t stream;
#endif
    // Pointer to the element in the buffer which is a pointer to the actual memory
    if(h_resultBuffer == nullptr || h_invResultBuffer == nullptr || h_phaseBuffer == nullptr)
        return false;
    // If buffer isn't initialized, initialize it
    if(*h_resultBuffer == nullptr)
        cudaMallocHost((void**)h_resultBuffer, sizeof(cufftComplex)*width*height);
    if(*h_invResultBuffer == nullptr && calcInv)
        cudaMallocHost((void**)h_invResultBuffer, sizeof(cufftComplex)*width*height);
    if(*h_phaseBuffer == nullptr)
        cudaMallocHost((void**)h_phaseBuffer, sizeof(float)*width*height);

    // send image to gpu
#if USE_EVENTS
    GPUs[currentDevice]->enqueueImage<T>(data, *h_resultBuffer, calcInv ? *h_invResultBuffer : nullptr,
                                      cFilter, *h_phaseBuffer, scaling,
                                      fftFillEvent, invFillEvent, phaseFillEvent, b_shiftFFT);
#else
    GPUs[currentDevice]->enqueueImage<T>(data, *h_resultBuffer, calcInv ? *h_invResultBuffer : nullptr,
                                         cFilter, *h_phaseBuffer, scaling, stream, b_shiftFFT);
    *fftFillStream = stream;
    *invFillStream = stream;
    *phaseFillStream = stream;
#endif
    std::cout << "Buffer size: " << h_resultBuffers[channel].getBacklogSize() << " " << h_invResultBuffers[channel].getBacklogSize() << " " << h_phaseBuffers[channel].getBacklogSize() << std::endl;
    ++currentDevice;
    if(currentDevice == GPUs.size())
        currentDevice = 0;
    return true;
}
// *********************************** Result fetching functions ***********************************************
// TODO: Need to use cudaEvents to know when a buffer has been filled with actual data, otherwise return false for reading
bool FFTDispatcher::getFFT(float* h_FFT, int channel)
{
    if(!h_invResultBuffers[channel].backValid())
        return false;
    cufftComplex** ptr = h_resultBuffers[channel].getBack();
    if(ptr == nullptr)
        return false;
    if(*ptr == nullptr)
        return false;
    for(int i =0; i < width*height; ++i)
    {
        h_FFT[i] = (*ptr)[i].x;
    }
    return true;
}

bool FFTDispatcher::getInvFFT(float* h_invFFT, int channel)
{
    if(!h_invResultBuffers[channel].backValid())
        return false;
    cufftComplex** ptr = h_invResultBuffers[channel].getBack();
    if(ptr == nullptr)
        return false;
    if(*ptr == nullptr)
        return false;
    for(int i =0; i < width*height; ++i)
    {
        h_invFFT[i] = (*ptr)[i].x;
    }
    return true;
}
bool FFTDispatcher::getPhase(float** h_phase, int channel)
{
    if(!h_phaseBuffers[channel].backValid())
    {
        std::cout << "Back not valid for channel " << channel << std::endl;
        return false;
    }
    //std::cout << "Getting phase for channel " << channel << " from buffer position ";
    float** h_phaseBuffer = h_phaseBuffers[channel].getBack();

    if(h_phaseBuffer == nullptr)
    {
        //  std::cout << " - position invalid\n";
        return false;
    }
    if(*h_phaseBuffer == nullptr)
    {
        //std::cout << " - position invalid\n";
        return false;
    }
    //std::cout << std::endl;
    /*auto maxVal = *std::max_element(*h_phaseBuffer, *h_phaseBuffer + width*height);
    maxVal = 1/ maxVal;
    for(int i = 0; i < width*height; ++i)
    {
        (*h_phaseBuffer)[i] *= maxVal;
    }*/
    *h_phase = *h_phaseBuffer;
    return true;
}
bool FFTDispatcher::shiftFFT(bool val)
{
    b_shiftFFT = val;
}
bool FFTDispatcher::setAsync(bool val)
{
    for(int i = 0; i < GPUs.size(); ++i)
    {
        GPUs[i]->asyncEnabled = val;
    }
    return true;
}

// ************************************************************************************************************
//											GPU Functions
// ************************************************************************************************************
// Multiply the amplitude of the input image by 1 - 2( x + y ) & 1
__global__
void fftShift(cufftComplex *data, unsigned nX, unsigned nY)
{
    unsigned int tid = threadIdx.x + (blockIdx.x * blockDim.x);
    ushort x = tid % nX;
    ushort y = (tid - x) / nX;

    if (x < nX && y < nY)
    {
        unsigned index = y * nX + x;
        float a = 1-2*((x+y)&1);
        data[index].x *= a;
        data[index].y *= a;
    }
}

__global__
void fftShift(cufftReal *data, unsigned nX, unsigned nY)
{
    unsigned int tid = threadIdx.x + (blockIdx.x * blockDim.x);
    ushort x = tid % nX;
    ushort y = (tid - x) / nX;

    if (x < nX && y < nY)
    {
        unsigned index = y * nX + x;
        float a = 1-2*((x+y)&1);
        data[index] *= a;
    }
}

// unshift and scale down
__global__
void ifftShift(cufftComplex *data, unsigned nX, unsigned nY, float scale)
{
    unsigned int tid = threadIdx.x + (blockIdx.x * blockDim.x);
    ushort x = tid % nX;
    ushort y = (tid - x) / nX;

    if (x < nX && y < nY)
    {
        unsigned index = y * nX + x;
        float a = (1-2*((x+y)&1)) /scale;
        data[index].x *= a;
        data[index].y *= a;
    }
}

// polar angle
__host__ __device__
float carg(const cuComplex& z) {return atan2(cuCimagf(z), cuCrealf(z));}
__host__ __device__
cuComplex cp2c(const float d, const float a) {return make_cuFloatComplex(d*cos(a), d*sin(a));}

template<bool inverted> __global__
void runFilter(cufftComplex* source, int pX, int pY, int C1, int C2) {
    unsigned int tid = threadIdx.x + (blockIdx.x * blockDim.x);
    ushort x = tid % pX;
    ushort y = (tid - x) / pX;
    bool b_outsideRegion;

    if( x < pX && y < pY )
    {
        float dx = abs(x - pX/2);
        dx *= dx;
        float dy = abs(y - pY/2);
        dy *= dy;
        b_outsideRegion = !(((dx+dy) >= C1) && ((dx+dy) <= C2));
        if( (!inverted && b_outsideRegion) || (inverted && !b_outsideRegion) )
        {
            int loc = x + y * pX;
            cufftComplex src = source[loc];
            source[loc] = cp2c(0, carg(src));
        }
    }
}
template<int scaling> __global__ void
getPhase(cufftComplex* source, float* phase)
{
    unsigned int tid = threadIdx.x + (blockIdx.x * blockDim.x);
    if(scaling == 0)
        phase[tid] = 100 * log(1.0 + cuCabsf(source[tid]));
    if(scaling == 1)
        phase[tid] = 100 * cuCabsf(source[tid]);
}

void __global__ convertU8toF32C2(unsigned char *pixels, cufftComplex *output)
{
    unsigned int tid = threadIdx.x + (blockIdx.x * blockDim.x);
    output[tid].x = pixels[tid];
}

template<typename T0, typename T1> void __global__ scaleImage(T0* image, T1 rows, T1 cols, T0 minVal)
{
    // First find the max across each row
    unsigned int row = threadIdx.y;
    extern __shared__ T0 max[];
    max[row] = minVal;
    for(int i  = 0; i < cols; ++i)
        if(image[i + row*cols] > max[row])
            max[row] = image[i + row*cols];
    __syncthreads();
    // Find the max of the max
    if(row == 0)
    {
        for(int i = 1; i < rows; ++i)
        {
            if(max[i] > max[0])
                max[0] = max[i];
        }
    }
    max[0] = 1.0 / max[0];
    __syncthreads();
    // Have each thread divide each element by the max
    for(int i = 0; i < cols; ++i)
    {
        image[i + row*cols] *= max[0];
    }
}


}// namespace AsyncFFT
// ************************************************************************************************************
//											Static C functions
// ************************************************************************************************************


static AsyncFFT::FFTDispatcher* g_dispatcher = 0;

char asyncFFT_init(int width, int height, int h_bufferSize, int d_bufferSize, const char* GPUs, int channels)
{
    // Parse the GPU input
    int devices;
    cudaGetDeviceCount(&devices);
    std::vector<int> vecGPUs;
    int i;
    std::stringstream ss(GPUs);
    while(ss >> i)
    {
        if(i >= devices)
            std::cout << "Device " << i << " is invalid, max device index: " << devices-1 << std::endl;
        else
        {
            cudaDeviceProp prop;
            cudaGetDeviceProperties(&prop,i);
            std::cout << "Using device " << prop.name <<std::endl;
            vecGPUs.push_back(i);
        }
        if(ss.peek() == ',')
            ss.ignore();
    }
    if(vecGPUs.size() == 0)
        return false;
    std::cout << "[AsyncFFT] initialized with " << vecGPUs.size() << " GPUs" << std::endl;
    if(g_dispatcher != 0)
        delete g_dispatcher;

    g_dispatcher = new AsyncFFT::FFTDispatcher(width, height, vecGPUs, channels, h_bufferSize, d_bufferSize);
    return true;
}


char asyncFFT_inputData(cufftComplex* data, int channel, char calcInverse, BandFilter* cFilter, int scaling)
{
    if(g_dispatcher == 0)
        return false;
    return g_dispatcher->dispatchImage(data, channel, calcInverse, cFilter, scaling);
}

char asyncFFT_inputPixelData(unsigned char* data, int channel, char calcInverse, BandFilter* cFilter, int scaling)
{
    if(g_dispatcher == 0)
        return false;
    return g_dispatcher->dispatchImage(data, channel, calcInverse, cFilter, scaling);
}

char asyncFFT_getFFT(float* data, int channel)
{
    if(g_dispatcher == 0)
        return false;
    return g_dispatcher->getFFT(data, channel);
}

char asyncFFT_getInvFFT(float* data, int channel)
{
    if(g_dispatcher == 0)
        return false;
    return g_dispatcher->getInvFFT(data, channel);
}

char asyncFFT_getPhase(float** data, int channel)
{
    if(g_dispatcher == 0)
        return false;
    return g_dispatcher->getPhase(data, channel);
}

char asyncFFT_getPhaseCopy(float* data, int channel)
{
    if(g_dispatcher == 0)
        return false;
    float* phaseBuffer;
    if(!g_dispatcher->getPhase(&phaseBuffer,channel))
        return false;
    std::memcpy(data,phaseBuffer,sizeof(float)*g_dispatcher->width*g_dispatcher->height);
    return true;
}
char asyncFFT_close()
{
    delete g_dispatcher;
    return true;
}
char asyncFFT_shiftFFT(char val)
{
    if(g_dispatcher == 0)
        return false;
    return g_dispatcher->shiftFFT(val);
}
char asyncFFT_setAsync(char val)
{
    if(g_dispatcher == 0)
        return false;
    return g_dispatcher->setAsync(val);
}
