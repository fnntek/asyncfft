Software repository for asyncFFT test code.  

AsyncFFT is a program that can take can multiplex FFTs to multiple GPU's in an Asynchronous fashion.  

Test program requires OpenCV for video io and display.  The cmake file includes looking for Qt since I built OpenCV with Qt support but it is not necessary if your build of OpenCV doesn't use Qt.

Includes various files from Mike's FFT implementation to allow testing and comparison to goYannis's previous FFT.